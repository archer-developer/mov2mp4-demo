import React, {useEffect, useRef} from 'react';
import { StyleSheet, View } from 'react-native';
import {RNCamera} from "react-native-camera";
import MovToMp4 from "react-native-mov-to-mp4";

export default function App() {

    const camera = useRef(null);

    const recordOptions = {
        mute: false,
        maxDuration: 5,
        quality: RNCamera.Constants.VideoQuality['288p'],
    };

    useEffect(() => {
        if (camera.current) {
            const promise = camera.current.recordAsync(recordOptions);

            if (promise) {
                promise.then((data) => {
                    console.warn('takeVideo', data);
                    const filename = Date.now().toString();
                    console.log('takeVideo', data.uri);
                    MovToMp4.convertMovToMp4(data.uri, filename)
                        .then(function (results) {
                            //here you can upload the video...
                            console.log(results);
                        });
                })
            }
        }
    }, camera);


    return (
        <View style={styles.container}>
            <RNCamera
                ref={camera}
                style={{
                    flex: 1,
                    width: 50,
                }}
            >
            </RNCamera>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
